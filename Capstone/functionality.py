from flask import Flask
from flask import render_template, g, request
from static.form import registerForm
from flask_sqlalchemy import SQLAlchemy
import sqlite3
from sqlite3 import Error, Connection
import os
from os import path
from markupsafe import Markup
#from flask_wtf.csrf import CSRFProtect


app = Flask(__name__)
#csrf = CSRFProtect(app)
#csrf.init_app(app)


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


#base route that we bridge off of
@app.route('/')
@app.route('/register', methods=['POST'])
def index():
    return render_template("login.html")

@app.route('/home')

def userLogin():
    return render_template("homepage.html")
#the following routes through nova are called when the buttons of base.html are clicked
@app.route('/m4a4')

def m4a4():
    return render_template("M4A4_skins.html")

@app.route('/USPS')

def usps():
    return render_template("USPS_skins.html")

@app.route('/AK')

def ak47():
    return render_template("AK_skins.html")

@app.route('/test')

def testing():
    return render_template("shoppingcart.html")

@app.route('/AWP')

def awp():
    return render_template("AWP_skins.html")

@app.route('/p2000')

def p2000():
    return render_template("p2000.html")

@app.route('/deag')

def deag():
    return render_template("deagle1.html")

@app.route('/negev')

def negev():
    return render_template("negev.html")

@app.route('/scout')

def scout():
    return render_template("scout.html")

@app.route('/m4a1-s')

def m4a1s():
    return render_template("m4a1-s.html")

@app.route('/nova')

def nova():
    return render_template("nova.html")

#function to fetch data on click of the wildfire awp skin
def wildfirenorm():
    conn = sqlite3.connect('../Capstone/static/AWP.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM `Wildfire`')
    result = cursor.fetchall()
    conn.close()
    return result
@app.route('/wildfire')
def WILDNORM():
  # (C1) GET ALL USERS
  skins = wildfirenorm()
  # (C2) RENDER HTML PAGE
  return render_template("/individualAWPpages/wildfire_skin.html", sk=skins) #goes to new page and displays table data

def neonoirnorm():
    conn = sqlite3.connect('../Capstone/static/AWP.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM `Neo Noir`')
    result = cursor.fetchall()
    conn.close()
    return result
@app.route('/neonoir')
def NEONORM():
  # (C1) GET ALL USERS
  skins = neonoirnorm()
  # (C2) RENDER HTML PAGE
  return render_template("/individualAWPpages/neonoir_skin.html", sk=skins)

def chromaticaberrationnorm():
    conn = sqlite3.connect('../Capstone/static/AWP.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM `Chromatic Aberration`')
    result = cursor.fetchall()
    conn.close()
    return result
@app.route('/chromaticaberration')
def CHRONORM():
  skins = chromaticaberrationnorm()
  return render_template("/individualAWPpages/chromaticaberration_skin.html", sk=skins)

def containmentbreachnorm():
    conn = sqlite3.connect('../Capstone/static/AWP.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM `Containment Breach`')
    result = cursor.fetchall()
    conn.close()
    return result
@app.route('/containmentbreach')
def CONTNORM():
  skins = containmentbreachnorm()
  return render_template("/individualAWPpages/containmentbreach_skin.html", sk=skins)



@app.route('/userLogin', methods=['POST', 'GET'])
def register():                                                                               #Function to add and verify users
    form = registerForm()
    if request.method == 'POST':                                                              #Checking for HTTP method (Register)
        if form.email.data != '' and form.password.data != '' and form.userName.data != '':   #Ensure that all html inputs aren't empty strings
               #Creating a table in database to store input
            try:
                connection = sqlite3.connect("/Capstone/static/userLoginInfo.db")
                #varibles from form info
                email = form.email.data
                password = form.password.data
                user = form.userName.data
                sql ="""INSERT INTO user_info
                        (Email, Password, User_Name)
                        VALUES(:Email, :Password, :User.data)"""
                args = {'Email': email, 'Password': password, 'User.data':user}
                connection.execute(sql, args)

            except Exception as e: # if there is a failure with the insert statement run above
                print(e)
                connection.rollback()
            else:
                # name_fail = 'username taken'
                connection.commit() # this 'saves' changes to our database, same as the Write button does in DB Browser
            finally:
                if connection != None: # this will make sure we close our connection if it has not already been closed - some statements like the 'with' keyword in python implicitly run __close__() at the end of the indented blocks
                    connection.close()
                print("done with insert")
                return render_template('login.html')

@app.route('/userLogin', methods=['POST', 'GET'])
def login():                                                                               #Function to add and verify users
    form = registerForm()
    if request.method == 'GET':                                                             #Checking for HTTP method (Login)
        if form.password.data != '' and form.email.data != '':                                #Ensure that all html inputs aren't empty strings
            #Search database and find the column that matches user input
            search = (f"""SELECT Email, Password FROM user_info
                        WHERE Email == '{form.email.data}'
                        AND Password == '{form.password.data}'""")
            #if entry exist grant access 
        else:
            #if entry does not exist deny access
            
            return render_template('login.html')
    return render_template('login.html')


@app.route('/update', methods=['POST', 'GET'])
def delete():                                                                               #Function to add and verify users
    form = registerForm()
    if request.method == 'POST':                                                                #Checking for HTTP method (Delete user)
        email = form.email.data
        password = form.password.data
        user = form.userName.data
        
        #Deleting user from dataBase
        try:
            connection = sqlite3.connect("C:/Users/Student/Programin/Capstone/capstonesillimanvicuna/Capstone/static/userLoginInfo.db")
            delete = ("""DELETE FROM user_info
                        WHERE Email = :form.email.data AND Password = :form.password.data AND User_Name = :form.userName.data""")
            args = {'form.email.data': email, 'form.password.data': password, 'form.userName.data':user}
            connection.execute(delete, args)
            

        except Exception as e: # if there is a failure with the insert statement run above
                print(e)
                connection.rollback()
        else:
            connection.commit() # this 'saves' changes to our database, same as the Write button does in DB Browser
        finally:
            return render_template('update.html') #<<<<<<change





@app.route('/update', methods=['POST', 'GET'])
def update():                                                                               #Function to add and verify users
    form = registerForm()
    if request.method == 'POST':                                                              #Checking for HTTP method (Register)
        if form.email.data != '' and form.password.data != '' and form.userName.data != '':   #Ensure that all html inputs aren't empty strings
            email = form.email.data
            password = form.password.data
            user = form.userName.data
        #Deleting user from dataBase
        try:
            connection = sqlite3.connect("C:/Users/Student/Programin/Capstone/capstonesillimanvicuna/Capstone/static/userLoginInfo.db")
            update =  ("""UPDATE user_info 
                        SET Password == :form.password.data
                       WHERE Email = :form.email.data AND User_Name = :form.userName.data""")

            args = {'form.email.data': email, 'form.password.data': password, 'form.userName.data':user}
            connection.execute(update, args)
        except Exception as e: # if there is a failure with the insert statement run above
                print(e)
                connection.rollback()
        else:
            connection.commit() # this 'saves' changes to our database, same as the Write button does in DB Browser
        finally:
            connection.close()    
            return render_template('login.html') #<<<<<<change
    else:    
        return render_template('login.html')

