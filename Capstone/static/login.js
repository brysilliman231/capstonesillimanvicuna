const wrapper = document.querySelector('.wrapper');


const loginLink = document.querySelector('.login-link');        //login botton click transition
loginLink.addEventListener('click', ()=> {
    wrapper.classList.remove('active');
});

const registerLink = document.querySelector('.register-link');  //register botton click transition
registerLink.addEventListener('click', ()=> {
    wrapper.classList.add('active');
});

const btnPopup = document.querySelector('.bntLogin-popup');     //Login popup to pull up login form (doesn't work)
btnPopup.addEventListener('click', ()=> {
    wrapper.classList.add('active-popup');
});

const iconClose = document.querySelector('.icon-close');        //Close icon to close login form (doesn't work)
iconClose.addEventListener('click', ()=> {
    wrapper.classList.remove('active-popup');
});

// const buttonRequest = document.querySelector('.btn');        //login and register button
// btn.addEventListener('click', ()=> {
//     wrapper.classList.remove('active');
// });


